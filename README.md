# Demo application for parsing text into sentences and words

This program can convert text into either XML formatted data, comma separated (CSV) data or JSON.
The text is to be parsed, broken into sentences and words and the words have to be sorted

###GRADLE compile and run
```
 ./gradlew bootRun -Pargs="-type json -input src/main/resources/large.in -output build/sentences.json"
```

###GRADLE build package tests check
```
 ./gradlew build
```

###run unix
```
build/install/demo-sentence/bin/demo-sentence [options]
```

###run windows
```
build\install\demo-sentence\bin\demo-sentence.bat [options]
```

###run oneJar
```
java -jar build/libs/demo-sentence*.jar [options]
```

###command line usage
```
Usage: demo-sentence [options]
  Options:
    -input
       if missing will use stdin
    -output
       if missing will use stdout
  * -type
       Options: [CSV, XML, JSON]
       Possible Values: [CSV, XML, JSON]
```