package net.devopssolutions.demo.sentence;

public interface MainEntryPoint {

    static void main(String[] args) throws Exception {
        new SentencesProcessor().run(args);
    }

}
