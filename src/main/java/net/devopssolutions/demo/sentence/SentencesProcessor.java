package net.devopssolutions.demo.sentence;

import lombok.extern.slf4j.Slf4j;
import net.devopssolutions.demo.sentence.config.Options;
import net.devopssolutions.demo.sentence.reader.SentenceReader;
import net.devopssolutions.demo.sentence.reader.SimpleSentenceReader;
import net.devopssolutions.demo.sentence.writer.OutputWriter;
import net.devopssolutions.demo.sentence.writer.SentenceSequenceWriter;

import java.io.*;
import java.util.Arrays;

@Slf4j
public class SentencesProcessor {

    public void run(String... args) throws Exception {
        log.info("application started args: {}", Arrays.asList(args));

        final Options options = Options.parseArguments(args);
        run(options);
    }

    public void run(Options options) throws Exception {
        log.info("application started options: {}", options);

        final Options.OutputType outputType = options.getOutputType();
        final InputStream inputStream = getInputStream(options);
        final OutputStream outputStream = getOutputStream(options);
        final OutputWriter outputWriter = outputType.getOutputWriter();
        final SentenceReader sentenceReader = new SimpleSentenceReader();

        produceOutput(inputStream, sentenceReader, outputType.name().toLowerCase(), outputWriter, outputStream);

        log.info("application stopped");
    }

    private OutputStream getOutputStream(Options options) throws FileNotFoundException {
        if (options.getOutputPath() == null) {
            return System.out;
        } else {
            return new FileOutputStream(options.getOutputPath());
        }
    }

    private InputStream getInputStream(Options options) throws FileNotFoundException {
        if (options.getInputPath() == null) {
            return System.in;
        } else {
            return new FileInputStream(options.getInputPath());
        }
    }

    private void produceOutput(InputStream inputStream, SentenceReader sentenceReader, String implementation, OutputWriter outputWriter, OutputStream outputStream) throws IOException {
        log.info("convert sentences to {}", implementation);
        try (SentenceSequenceWriter sentenceSequenceWriter = outputWriter.sequenceWriter(outputStream)) {
            sentenceReader.convertSentences(inputStream, sentenceSequenceWriter::writeSentence);
        } finally {
            outputStream.close();
        }
    }

}
