package net.devopssolutions.demo.sentence.config;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import lombok.Data;
import net.devopssolutions.demo.sentence.writer.CsvOutputWriter;
import net.devopssolutions.demo.sentence.writer.JsonOutputWriter;
import net.devopssolutions.demo.sentence.writer.OutputWriter;
import net.devopssolutions.demo.sentence.writer.XmlOutputWriter;

@Data
public class Options {

    @Parameter(names = "-input", arity = 1, description = "if missing will use stdin")
    private String inputPath;

    @Parameter(names = "-output", arity = 1, description = "if missing will use stdout")
    private String outputPath;

    @Parameter(names = "-type", arity = 1, converter = TypeStringConverter.class, required = true)
    private OutputType outputType;

    public static Options parseArguments(String... args) throws Exception {
        final Options options = new Options();
        final JCommander jCommander = new JCommander(options);
        jCommander.setProgramName("demo-sentence");
        try {
            jCommander.parse(args);
            return options;
        } catch (Exception e) {
            jCommander.usage();
            System.out.flush();
            Thread.sleep(100);
            throw e;
        }
    }

    public enum OutputType {
        CSV,
        XML,
        JSON;

        public OutputWriter getOutputWriter() {
            OutputWriter outputWriter = null;
            switch (this) {
                case CSV:
                    outputWriter = new CsvOutputWriter();
                    break;
                case XML:
                    outputWriter = new XmlOutputWriter();
                    break;
                case JSON:
                    outputWriter = new JsonOutputWriter();
                    break;
            }
            return outputWriter;
        }
    }

    public static class TypeStringConverter implements IStringConverter<OutputType> {

        @Override
        public OutputType convert(String value) {
            return OutputType.valueOf(value.toUpperCase());
        }
    }
}
