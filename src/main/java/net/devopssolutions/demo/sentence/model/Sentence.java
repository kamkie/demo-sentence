package net.devopssolutions.demo.sentence.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Collections;
import java.util.List;

@Getter
@ToString
@EqualsAndHashCode
@JacksonXmlRootElement(localName = "sentence")
public class Sentence {

    @JsonProperty(required = true, value = "sentence")
    @JacksonXmlProperty(localName = "word")
    @JacksonXmlElementWrapper(useWrapping = false)
    private final List<String> words;

    public Sentence(@JsonProperty(value = "word") List<String> words) {
        words.sort((o1, o2) -> {
            if (o1.equalsIgnoreCase(o2) && !o1.equals(o2)) {
                return o1.compareTo(o2);
            } else {
                return o1.compareToIgnoreCase(o2);
            }
        });
        this.words = Collections.unmodifiableList(words);
    }

}
