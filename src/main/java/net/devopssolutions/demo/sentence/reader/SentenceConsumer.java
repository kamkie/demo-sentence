package net.devopssolutions.demo.sentence.reader;

import net.devopssolutions.demo.sentence.model.Sentence;

import java.io.IOException;

public interface SentenceConsumer {

    void consume(Sentence sentence) throws IOException;

}
