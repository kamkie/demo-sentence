package net.devopssolutions.demo.sentence.reader;

import net.devopssolutions.demo.sentence.model.Sentence;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface SentenceReader {

    List<Sentence> readSentences(InputStream inputStream) throws IOException;

    void convertSentences(InputStream inputStream, SentenceConsumer sentenceConsumer) throws IOException;

}
