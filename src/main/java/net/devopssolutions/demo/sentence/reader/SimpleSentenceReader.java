package net.devopssolutions.demo.sentence.reader;

import lombok.extern.slf4j.Slf4j;
import net.devopssolutions.demo.sentence.model.Sentence;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

@Slf4j
public class SimpleSentenceReader implements SentenceReader {

    protected static final Pattern PUNCTUATION_PATTERN = Pattern.compile("[\\p{Punct}&&[^\u0027]]");
    protected static final Pattern SPLIT_PATTERN = Pattern.compile("\\s+");
    protected static final Pattern REPLACE_SPECIAL_CHARACTER_PATTERN = Pattern.compile("\u2019");

    protected final BidiMap<String, String> substitutions = new DualHashBidiMap<>();

    public SimpleSentenceReader() {
        substitutions.put("Mr.", "Mr");
    }

    @Override
    public List<Sentence> readSentences(InputStream inputStream) throws IOException {
        final List<Sentence> sentences = new ArrayList<>();

        convertSentences(inputStream, sentences::add);

        return sentences;
    }

    @Override
    public void convertSentences(InputStream inputStream, SentenceConsumer sentenceConsumer) throws IOException {
        try (Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name())) {
            List<String> words = new ArrayList<>();

            while (scanner.hasNext()) {
                boolean isSentenceEnd = readWords(scanner.next(), words);

                if (isSentenceEnd || !scanner.hasNext()) {
                    sentenceConsumer.consume(new Sentence(words));
                    words = new ArrayList<>();
                }
            }
        }
    }

    protected boolean readWords(String next, List<String> words) {
        boolean isSentenceEnd = isSentenceEnd(next.trim());

        next = PUNCTUATION_PATTERN.matcher(next).replaceAll(" ");

        String[] split = SPLIT_PATTERN.split(next);
        for (String s : split) {
            if (StringUtils.isNotBlank(s)) {
                s = REPLACE_SPECIAL_CHARACTER_PATTERN.matcher(s.trim()).replaceAll("\\'");
                if (substitutions.values().contains(s)) {
                    s = substitutions.getKey(s);
                }
                words.add(s);
            }
        }
        return isSentenceEnd;
    }

    protected boolean isSentenceEnd(String word) {
        return (word.endsWith(".") || word.endsWith("?") || word.endsWith("!"))
                && !substitutions.keySet().contains(word);
    }

}
