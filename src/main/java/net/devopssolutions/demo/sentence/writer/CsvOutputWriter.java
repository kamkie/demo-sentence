package net.devopssolutions.demo.sentence.writer;

import net.devopssolutions.demo.sentence.model.Sentence;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class CsvOutputWriter implements OutputWriter {

    private CSVPrinter csvPrinter(OutputStream outputStream) throws IOException {
        return new CSVPrinter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8), CSVFormat.DEFAULT.withRecordSeparator("\r\n"));
    }

    @Override
    public void writeSingleSentence(OutputStream outputStream, Sentence sentence) throws IOException {
        writeSingleSentence(outputStream, sentence, null);
    }

    @Override
    public void writeSentenceList(OutputStream outputStream, List<Sentence> sentences) throws IOException {
        try (CsvSentenceSequenceWriter csvSentenceSequenceWriter = new CsvSentenceSequenceWriter(outputStream)) {
            for (Sentence sentence : sentences) {
                csvSentenceSequenceWriter.writeSentence(sentence);
            }
        }
    }

    public void writeSingleSentence(OutputStream outputStream, Sentence sentence, Integer number) throws IOException {
        writeSingleSentence(csvPrinter(outputStream), sentence, number);
    }

    public void writeSingleSentence(CSVPrinter csvPrinter, Sentence sentence, Integer number) throws IOException {
        if (number == null) {
            csvPrinter.print("Sentence");
        } else {
            csvPrinter.print("Sentence " + number);
        }
        csvPrinter.printRecord(sentence.getWords());
        csvPrinter.flush();
    }

    @Override
    public SentenceSequenceWriter sequenceWriter(OutputStream outputStream) throws IOException {
        return new CsvSentenceSequenceWriter(outputStream);
    }

    private class CsvSentenceSequenceWriter implements SentenceSequenceWriter {
        private final CSVPrinter csvPrinter;
        private int number = 1;

        public CsvSentenceSequenceWriter(OutputStream outputStream) throws IOException {
            csvPrinter = csvPrinter(outputStream);
            csvPrinter.print("");
            for (int i = 1; i < 34; i++) {
                csvPrinter.print("Word " + i);
            }
            csvPrinter.println();
            csvPrinter.flush();
        }

        @Override
        public void writeSentence(Sentence sentence) throws IOException {
            writeSingleSentence(csvPrinter, sentence, number);
            number++;
        }

        @Override
        public void close() throws IOException {
            csvPrinter.close();
        }
    }
}
