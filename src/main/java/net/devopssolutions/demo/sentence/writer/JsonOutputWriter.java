package net.devopssolutions.demo.sentence.writer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SequenceWriter;
import net.devopssolutions.demo.sentence.model.Sentence;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class JsonOutputWriter implements OutputWriter {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void writeSingleSentence(OutputStream outputStream, Sentence sentence) throws IOException {
        objectMapper.writeValue(outputStream, sentence);
    }

    @Override
    public void writeSentenceList(OutputStream outputStream, List<Sentence> sentences) throws IOException {
        objectMapper.writeValue(outputStream, sentences);
    }

    public SentenceSequenceWriter sequenceWriter(OutputStream outputStream) throws IOException {
        return new JsonSentenceSequenceWriter(outputStream, objectMapper);
    }

    private static class JsonSentenceSequenceWriter implements SentenceSequenceWriter {
        private final SequenceWriter sequenceWriter;

        public JsonSentenceSequenceWriter(OutputStream outputStream, ObjectMapper objectMapper) throws IOException {
            this.sequenceWriter = objectMapper.writerWithDefaultPrettyPrinter()
                    .writeValuesAsArray(outputStream);
        }

        @Override
        public void writeSentence(Sentence sentence) throws IOException {
            sequenceWriter.write(sentence);
        }

        @Override
        public void close() throws IOException {
            sequenceWriter.close();
        }
    }

}
