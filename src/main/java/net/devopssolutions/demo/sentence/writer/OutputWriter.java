package net.devopssolutions.demo.sentence.writer;

import net.devopssolutions.demo.sentence.model.Sentence;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public interface OutputWriter {

    /**
     * outputStream is not closed
     *
     * @param outputStream
     * @param sentence
     * @throws IOException
     */
    void writeSingleSentence(OutputStream outputStream, Sentence sentence) throws IOException;


    void writeSentenceList(OutputStream outputStream, List<Sentence> sentences) throws IOException;

    SentenceSequenceWriter sequenceWriter(OutputStream outputStream) throws IOException;

}
