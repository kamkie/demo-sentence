package net.devopssolutions.demo.sentence.writer;

import net.devopssolutions.demo.sentence.model.Sentence;

import java.io.Closeable;
import java.io.IOException;

public interface SentenceSequenceWriter extends Closeable {

    void writeSentence(Sentence sentence) throws IOException;

}
