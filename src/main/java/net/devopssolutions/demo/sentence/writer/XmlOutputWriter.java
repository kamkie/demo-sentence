package net.devopssolutions.demo.sentence.writer;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.devopssolutions.demo.sentence.model.Sentence;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Slf4j
public class XmlOutputWriter implements OutputWriter {

    private static final JavaType SENTENCE_TYPE = TypeFactory.defaultInstance().constructType(Sentence.class);

    private final XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
    private final XmlMapper xmlMapper = new XmlMapper();
    private final ObjectWriter objectWriter;

    public XmlOutputWriter() {
        objectWriter = xmlMapper.writer();
    }

    @Override
    public void writeSingleSentence(OutputStream outputStream, Sentence sentence) throws IOException {
        objectWriter.writeValue(outputStream, sentence);
    }

    @Override
    public void writeSentenceList(OutputStream outputStream, List<Sentence> sentences) throws IOException {
        try (XmlSentenceSequenceWriter xmlSentenceSequenceWriter = new XmlSentenceSequenceWriter(outputStream)) {
            for (Sentence sentence : sentences) {
                log.debug("writing document {}", sentence);
                xmlSentenceSequenceWriter.writeSentence(sentence);
            }
        }
    }

    @Override
    public SentenceSequenceWriter sequenceWriter(OutputStream outputStream) throws IOException {
        return new XmlSentenceSequenceWriter(outputStream);
    }

    private class XmlSentenceSequenceWriter implements SentenceSequenceWriter {
        private final OutputStream outputStream;
        private final XMLStreamWriter xmlStreamWriter;

        @SneakyThrows
        public XmlSentenceSequenceWriter(OutputStream outputStream) throws IOException {
            this.outputStream = outputStream;

            this.xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(outputStream);
            this.xmlStreamWriter.writeStartDocument("UTF-8", "1.0");
            this.xmlStreamWriter.writeStartElement("text");
            this.xmlStreamWriter.writeCharacters("");
            this.xmlStreamWriter.flush();
        }

        @Override
        public void writeSentence(Sentence sentence) throws IOException {
            log.debug("writing sentence {}", sentence);
            SequenceWriter sequenceWriter = objectWriter.writeValuesAsArray(outputStream);
            sequenceWriter.write(sentence, SENTENCE_TYPE);
            sequenceWriter.flush();
        }

        @Override
        @SneakyThrows
        public void close() throws IOException {
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeEndDocument();
            xmlStreamWriter.close();
            outputStream.close();
        }
    }
}
