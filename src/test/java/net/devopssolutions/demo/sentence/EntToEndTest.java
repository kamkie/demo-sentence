package net.devopssolutions.demo.sentence;

import com.beust.jcommander.ParameterException;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class EntToEndTest {

    private final ClassLoader classLoader = MainEntryPoint.class.getClassLoader();
    private final URL resource = classLoader.getResource("small.in");

    @Test
    public void testXml() throws Exception {
        Assert.assertNotNull(resource);
        new SentencesProcessor().run("-type", "xml", "-input", resource.getFile(), "-output", "build/sentences." + "xml");
    }

    @Test
    public void testCsv() throws Exception {
        Assert.assertNotNull(resource);
        new SentencesProcessor().run("-type", "csv", "-input", resource.getFile(), "-output", "build/sentences." + "csv");
    }

    @Test
    public void testJson() throws Exception {
        Assert.assertNotNull(resource);
        new SentencesProcessor().run("-type", "json", "-input", resource.getFile(), "-output", "build/sentences." + "json");
    }

    @Test
    public void testStd() throws Exception {
        Assert.assertNotNull(resource);

        InputStream in = System.in;
        PrintStream out = System.out;

        System.setIn(resource.openStream());
        System.setOut(new NullPrintStream());

        new SentencesProcessor().run("-type", "json");

        System.setIn(in);
        System.setOut(out);
    }

    @Test(expected = NullPointerException.class)
    public void testFail() throws Exception {
        MainEntryPoint.main(null);
    }

    @Test(expected = ParameterException.class)
    public void testFail2() throws Exception {
        MainEntryPoint.main(new String[]{});
    }

    private static class NullPrintStream extends PrintStream {
        public NullPrintStream() throws UnsupportedEncodingException {
            super(new NullByteArrayOutputStream(), false, StandardCharsets.UTF_8.name());
        }
    }

    private static class NullByteArrayOutputStream extends ByteArrayOutputStream {
        @Override
        public void write(int b) {
        }

        @Override
        public void write(byte[] b, int off, int len) {
        }

        @Override
        public void writeTo(OutputStream out) throws IOException {
        }
    }

}