package net.devopssolutions.demo.sentence.config;

import com.beust.jcommander.ParameterException;
import org.junit.Assert;
import org.junit.Test;

public class OptionsTest {

    @Test
    public void testParseType() throws Exception {
        Options options = Options.parseArguments("-type", "CSV");

        Assert.assertEquals(Options.OutputType.CSV, options.getOutputType());
    }

    @Test
    public void testParseTypeAndInput() throws Exception {
        Options options = Options.parseArguments("-type", "json", "-input", "/pat/to/input/file");

        Assert.assertEquals(Options.OutputType.JSON, options.getOutputType());
        Assert.assertEquals("/pat/to/input/file", options.getInputPath());
    }

    @Test
    public void testParseTypeAndOutput() throws Exception {
        Options options = Options.parseArguments("-type", "json", "-output", "/pat/to/output/file");

        Assert.assertEquals(Options.OutputType.JSON, options.getOutputType());
        Assert.assertEquals("/pat/to/output/file", options.getOutputPath());
    }

    @Test
    public void testParseTypeInputOutput() throws Exception {
        Options options = Options.parseArguments("-type", "json", "-input", "/pat/to/input/file", "-output", "/pat/to/output/file");

        Assert.assertEquals(Options.OutputType.JSON, options.getOutputType());
        Assert.assertEquals("/pat/to/input/file", options.getInputPath());
        Assert.assertEquals("/pat/to/output/file", options.getOutputPath());
    }

    @Test(expected = ParameterException.class)
    public void testFailAdditionalParameter() throws Exception {
        Options.parseArguments("-type", "CSV", "json");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailWrongValue() throws Exception {
        Options.parseArguments("-type", "ble");
    }

    @Test(expected = ParameterException.class)
    public void testMissingParameter() throws Exception {
        Options.parseArguments();
    }

}