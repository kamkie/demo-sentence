package net.devopssolutions.demo.sentence.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Collections;
import java.util.List;

@Getter
@EqualsAndHashCode
@ToString
@JacksonXmlRootElement(localName = "text")
public class Text {

    @JsonProperty(required = true, value = "sentence")
    @JacksonXmlElementWrapper(useWrapping = false)
    private final List<Sentence> sentences;

    @JsonCreator
    public Text(@JsonProperty("sentence") List<Sentence> sentences) {
        this.sentences = Collections.unmodifiableList(sentences);
    }

}
