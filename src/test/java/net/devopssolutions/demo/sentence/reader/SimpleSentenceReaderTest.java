package net.devopssolutions.demo.sentence.reader;

import net.devopssolutions.demo.sentence.model.Sentence;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class SimpleSentenceReaderTest {

    private final ClassLoader classLoader = getClass().getClassLoader();
    private SentenceReader sentenceReader = new SimpleSentenceReader();

    @Test
    public void testReadSentence() throws Exception {
        URL resource = classLoader.getResource("small.in");
        assert resource != null;
        InputStream inputStream = resource.openStream();

        List<Sentence> sentences = sentenceReader.readSentences(inputStream);

        Assert.assertNotNull(sentences);
        Assert.assertEquals(13, sentences.size());
        Assert.assertEquals(new Sentence(Arrays.asList("he", "shocking", "shouted", "was", "What", "你这肮脏的掠夺者", "停在那儿")), sentences.get(0));
        Assert.assertEquals(new Sentence(Arrays.asList("a", "because", "Chinese", "couldn't", "I", "isn't", "mother", "my", "perhaps", "tongue", "understand", "word")), sentences.get(1));
        Assert.assertEquals(new Sentence(Arrays.asList("around", "furious", "he", "I", "just", "marching", "Mr.", "standing", "there", "was", "was", "watching", "Young")), sentences.get(2));
        Assert.assertEquals(new Sentence(Arrays.asList("anger", "at", "directing", "he", "his", "me", "was", "Why")), sentences.get(3));
        Assert.assertEquals(new Sentence(Arrays.asList("about", "did", "I", "know", "Little", "that")), sentences.get(4));
        Assert.assertEquals(new Sentence(Arrays.asList("and", "and", "Baltic", "banking", "capital", "in", "international", "investment", "is", "leading", "markets", "Markets", "Nordea", "Nordic", "operator", "partner", "regions", "Sea", "the", "the")), sentences.get(5));
        Assert.assertEquals(new Sentence(Arrays.asList("are", "connecting", "door", "global", "located", "markets", "next", "the", "to", "to", "We", "you", "you")), sentences.get(6));
        Assert.assertEquals(new Sentence(Arrays.asList("a", "and", "combine", "complete", "expertise", "financial", "global", "local", "of", "portfolio", "provide", "services", "solutions", "strength", "to", "We", "with", "with", "you")), sentences.get(7));
        Assert.assertEquals(new Sentence(Arrays.asList("and", "currencies", "diversified", "have", "in", "in", "liquidity", "local", "most", "Nordics", "of", "offer", "one", "outstanding", "product", "ranges", "strongest", "the", "the", "We")), sentences.get(8));
        Assert.assertEquals(new Sentence(Arrays.asList("access", "all", "an", "best", "But", "capital", "dedicated", "experts", "facets", "in", "in", "manner", "markets", "more", "of", "of", "offer", "possible", "serving", "significantly", "team", "the", "to", "to", "unequalled", "we", "you", "you")), sentences.get(9));
        Assert.assertEquals(new Sentence(Arrays.asList("a", "a", "and", "and", "At", "combination", "customer", "expertise", "financial", "for", "gives", "global", "have", "local", "Markets", "Nordea", "of", "of", "opportunity", "our", "rare", "services", "solutions", "strength", "the", "to", "us", "use", "variety", "we", "which", "wide", "you")), sentences.get(10));
        Assert.assertEquals(new Sentence(Arrays.asList("a", "a", "all", "and", "and", "as", "as", "can", "currencies", "diversified", "excellent", "fact", "finding", "give", "hard", "have", "in", "in", "In", "liquidity", "local", "Nordics", "of", "ours", "product", "range", "strong", "the", "time", "too", "we", "you", "you'd")), sentences.get(11));
        Assert.assertEquals(new Sentence(Arrays.asList("a", "be", "But", "challenge", "financial", "have", "huge", "importantly", "matter", "might", "most", "no", "of", "outstanding", "ready", "serve", "specialists", "team", "to", "we", "what", "you", "your")), sentences.get(12));
    }
}