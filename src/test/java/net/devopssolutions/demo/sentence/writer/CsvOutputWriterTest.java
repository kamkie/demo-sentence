package net.devopssolutions.demo.sentence.writer;

import lombok.extern.slf4j.Slf4j;
import net.devopssolutions.demo.sentence.model.Sentence;
import net.devopssolutions.demo.sentence.reader.SentenceReader;
import net.devopssolutions.demo.sentence.reader.SimpleSentenceReader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class CsvOutputWriterTest {

    private final ClassLoader classLoader = getClass().getClassLoader();
    private final static String EXPECTED = "\"\",Word 1,Word 2,Word 3,Word 4,Word 5,Word 6,Word 7,Word 8,Word 9,Word 10,Word 11,Word 12,Word 13,Word 14,Word 15,Word 16,Word 17,Word 18,Word 19,Word 20,Word 21,Word 22,Word 23,Word 24,Word 25,Word 26,Word 27,Word 28,Word 29,Word 30,Word 31,Word 32,Word 33\r\n" +
            "Sentence 1,aaa,ble\r\n" +
            "Sentence 2,aaa,ble";
    private OutputWriter outputWriter = new CsvOutputWriter();

    @Test
    public void testWriteSingleSentence() throws Exception {
        String output = TestUtil.serializeSingle(outputWriter);

        Assert.assertEquals("Sentence,aaa,ble", output.trim());
    }

    @Test
    public void testWriteSentenceList() throws Exception {
        String output = TestUtil.serializeList(outputWriter);

        Assert.assertEquals(EXPECTED, output.trim());
    }

    @Test
    public void testWrite() throws Exception {
        URL textInput = classLoader.getResource("small.in");
        assert textInput != null;
        URL sentencesCsv = classLoader.getResource("small.csv");
        assert sentencesCsv != null;

        SentenceReader sentenceReader = new SimpleSentenceReader();
        List<Sentence> sentencesFromText = sentenceReader.readSentences(textInput.openStream());
        String serializedSentencesFromText = TestUtil.serializeList(outputWriter, sentencesFromText);

        CSVParser csvParser = CSVParser.parse(sentencesCsv, StandardCharsets.UTF_8, CSVFormat.DEFAULT);
        List<CSVRecord> records = csvParser.getRecords();
        records.remove(0);

        List<Sentence> sentencesFromCsv = new ArrayList<>();
        for (CSVRecord record : records) {
            List<String> words = new ArrayList<>();
            for (String s : record) {
                s = s.trim();
                if (!s.contains(" ")) {
                    words.add(s);
                }
            }
            sentencesFromCsv.add(new Sentence(words));
        }

        String serializedSentencesFromCsv = TestUtil.serializeList(outputWriter, sentencesFromCsv);

        Assert.assertEquals(serializedSentencesFromCsv, serializedSentencesFromText);
        Assert.assertEquals(sentencesFromCsv, sentencesFromText);
    }

}