package net.devopssolutions.demo.sentence.writer;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

@Slf4j
public class JsonOutputWriterTest {

    private final OutputWriter outputWriter = new JsonOutputWriter();

    @Test
    public void testWrite() throws Exception {
        String output = TestUtil.serializeSingle(outputWriter);

        Assert.assertEquals("{\"sentence\":[\"aaa\",\"ble\"]}", output.trim());
    }

    @Test
    public void testWriteSentenceList() throws Exception {
        String output = TestUtil.serializeList(outputWriter);

        Assert.assertEquals("[{\"sentence\":[\"aaa\",\"ble\"]},{\"sentence\":[\"aaa\",\"ble\"]}]", output.trim());
    }

}