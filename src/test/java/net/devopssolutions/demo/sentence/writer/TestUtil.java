package net.devopssolutions.demo.sentence.writer;

import net.devopssolutions.demo.sentence.model.Sentence;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestUtil {

    public static String serializeList(OutputWriter outputWriter) throws IOException {
        Sentence sentence = new Sentence(Arrays.asList("ble", "aaa"));
        List<Sentence> sentences = new ArrayList<>();
        sentences.add(sentence);
        sentences.add(sentence);

        return serializeList(outputWriter, sentences);
    }

    public static String serializeList(OutputWriter outputWriter, List<Sentence> sentences) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        outputWriter.writeSentenceList(outputStream, sentences);

        return new String(outputStream.toByteArray(), StandardCharsets.UTF_8).trim();
    }

    public static String serializeSingle(OutputWriter outputWriter) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        outputWriter.writeSingleSentence(outputStream, new Sentence(Arrays.asList("ble", "aaa")));

        return new String(outputStream.toByteArray(), StandardCharsets.UTF_8).trim();
    }

}
