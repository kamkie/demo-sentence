package net.devopssolutions.demo.sentence.writer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import lombok.extern.slf4j.Slf4j;
import net.devopssolutions.demo.sentence.model.Sentence;
import net.devopssolutions.demo.sentence.model.Text;
import net.devopssolutions.demo.sentence.reader.SentenceReader;
import net.devopssolutions.demo.sentence.reader.SimpleSentenceReader;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;
import java.util.List;

@Slf4j
public class XmlOutputWriterTest {

    private final static String expected = "<sentence><word>aaa</word><word>ble</word></sentence>";
    private final static String expectedList = "<?xml version='1.0' encoding='UTF-8'?>" +
            "<text><sentence><word>aaa</word><word>ble</word></sentence>" +
            "<sentence><word>aaa</word><word>ble</word></sentence></text>";

    private final ClassLoader classLoader = getClass().getClassLoader();
    private final OutputWriter outputWriter = new XmlOutputWriter();

    @Test
    public void testWriteSingleSentence() throws Exception {
        String output = TestUtil.serializeSingle(outputWriter);

        Assert.assertEquals(expected, output.trim());
    }

    @Test
    public void testWriteSentenceList() throws Exception {
        String output = TestUtil.serializeList(outputWriter);

        Assert.assertEquals(expectedList, output.trim());
    }

    @Test
    public void testWrite() throws Exception {
        URL textInput = classLoader.getResource("small.in");
        assert textInput != null;
        URL sentencesXml = classLoader.getResource("small.xml");
        assert sentencesXml != null;

        SentenceReader sentenceReader = new SimpleSentenceReader();
        List<Sentence> sentencesFromText = sentenceReader.readSentences(textInput.openStream());
        String serializedSentencesFromText = TestUtil.serializeList(outputWriter, sentencesFromText);

        JacksonXmlModule module = new JacksonXmlModule();
        module.setDefaultUseWrapper(false);
        XmlMapper xmlMapper = new XmlMapper(module);
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);

        Text sentencesFromXml = xmlMapper.readValue(sentencesXml.openStream(), new TypeReference<Text>() {
        });

        String serializedSentencesFromXml = TestUtil.serializeList(outputWriter, sentencesFromXml.getSentences());

        Assert.assertEquals(serializedSentencesFromXml, serializedSentencesFromText);
        Assert.assertEquals(sentencesFromXml.getSentences(), sentencesFromText);
    }
}